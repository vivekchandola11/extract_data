import os
from pathlib import Path
development = True
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  #C:\Users\Dell\Desktop\coutoEditor

if development:
    Q_C = {'orm': 'default', 'sync': True}
    BASE_URL = 'http://localhost:8000/'
    # BASE_URL = 'C:/Users/Dell/Desktop/GETBOARDED/'

    # DATABASE = {
    #         'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #         'NAME': 'cricket',
    #         'USER': 'postgres',
    #         'PASSWORD': 'Sumit@123',
    #         'HOST': 'localhost',
    #         'PORT': '5432',
    #     }

    DATABASE = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }

    ROLLBAR_ACCESS = {
        'access_token': '3a684ebfdcc34ff69446d583519d5666'
    }

else:
    Q_C = {
        'name': 'sport_scraper',
        'workers': 2,
        'recycle': 500,
        'timeout': 1000000,
        'compress': True,
        'save_limit': 20,
        'queue_limit': 500,
        'cpu_affinity': 500,
        "sync": False,
        'label': 'Django Q',
        'redis': {
            'host': 'localhost',
            'port': 6379,
            'db': 0, },
        'error_reporter': {
            'rollbar': {
                'access_token': 'ffdd9a16cdb346b1a6a7295c1ee84cc6',
                'environment': 'Django-Q'
            }
        }
    }
    BASE_URL = "https://dev.api.videowiki.pt/"
    # DATABASE = {
    #         'ENGINE': 'django.db.backends.postgresql_psycopg2',
    #         'NAME': 'cricket',
    #         'USER': 'postgres',
    #         'PASSWORD': 'Sumit@123',
    #         'HOST': 'localhost',
    #         'PORT': '5432',
    #     }
    DATABASE = {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }

    ROLLBAR_ACCESS  = {
        'access_token': '3a684ebfdcc34ff69446d583519d2fa9',
        'environment': 'development' if development else 'production',
        'root': BASE_DIR,
    }
