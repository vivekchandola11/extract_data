from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from extractfile.working_api.extract_text import extract_text_from_file, extract_data_from_url
from rest_framework.response import Response

# from videos.models import TemporaryFiles
from .models import TemporaryFiles
from .serializer import FileSerializer


class ExtractDetailFromFile(APIView):

    def get(self, requests, format = None):
        s = TemporaryFiles.objects.all()
        ser = FileSerializer(s, many=True)
        return Response(ser.data)

    def post(self, requests, format = None):
        # file = TemporaryFiles.objects.create(temp_file = requests.data["file"]).temp_file
        file = TemporaryFiles.objects.create(temp_file = requests.data.get("file", None)).temp_file
        file1 = str(file)
        file_format = ['.pdf','.ppt','pptx','.doc','docx','.txt','.odt']
        if file1[-4:] in file_format:
            file_data = extract_text_from_file(file)
        else:
            file_data = extract_data_from_url(file)

        return Response({
            "data" : file_data.content
        })



