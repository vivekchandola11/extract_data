from django.urls import path, include
from extractfile.views import ExtractDetailFromFile


urlpatterns = [
    path(r'extract_info/', ExtractDetailFromFile.as_view())
]

