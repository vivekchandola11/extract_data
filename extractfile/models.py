from django.db import models
from django.utils import timezone

# Create your models here.
class TemporaryFiles(models.Model):
    created_at = models.DateTimeField(default = timezone.datetime.utcnow)
    temp_file = models.FileField(upload_to="temporary/%Y/%m/%d")

    def __str__(self):
        return str(self.temp_file)
    
