from rest_framework import serializers

from .models import TemporaryFiles

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemporaryFiles
        fields = "__all__"

